FROM debian

RUN apt update -y
RUN apt install python3-pip -y
RUN pip3 install Flask
COPY ./app /app
WORKDIR /app
CMD ["python3", "main.py"]
